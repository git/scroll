module Spell.Enum where

data SpellEnum
	= SpellVomit
	| SpellWhiteout
	| SpellDream
	| SpellNew
	| SpellGenocide
	| SpellBerzerk
	| SpellReverse
	| SpellWish
	deriving (Show, Eq, Ord)
