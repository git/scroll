module Help where

import Types
import Status

helpWindow :: M ()
helpWindow = showWindow (3,3) $ map (\l -> " " ++ l ++ " ")
	[ "Move: hjkl or arrow keys"
	, ""
	, "Navigate through the whitespace on your way down the scroll."
	, "Move over a letter to swallow it. (Punctuation is tasty too!)"
	, "You cannot move entirely past some swallowed letters."
	, "(Back up if you get stuck!)"
	, ""
	, "d: Dive to other side of scroll"
	, "TAB: Force screen refresh"
	, "Q: Quit the game"
	, "i: Spell inventory"
	, ""
	, "To cast a spell, swallow the spell components,"
	, "and type the magic letter or phrase."
	, ""
	, "Scroll is a 7DRL 2015 entry! And a file pager..."
	, "GPL-2+ Copyright 2015 by Joey Hess <scroll@joeyh.name>"
	]
