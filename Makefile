all:
	cabal build
	cabal install --installdir=. --overwrite-policy=always

hackage:
	@cabal sdist
	@cabal upload dist/*.tar.gz

clean:
	cabal clean
