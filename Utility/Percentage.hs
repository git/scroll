{- percentages
 -
 - Copyright 2012 Joey Hess <id@joeyh.name>
 -
 - License: BSD-2-clause
 -}

module Utility.Percentage (
	Percentage,
	percentage,
	showPercentage
) where

import Data.Ratio

newtype Percentage = Percentage (Ratio Integer)

instance Show Percentage where
	show = showPercentage

{- Normally the big number comes first. But 110% is allowed if desired. :) -}
percentage :: Integer -> Integer -> Percentage
percentage 0 _ = Percentage 0
percentage full have = Percentage $ have * 100 % full

{- Pretty-print a Percentage, with a specified level of precision. -}
showPercentage :: Percentage -> String
showPercentage (Percentage p) = show n ++ "%"
  where
	n = floor (fromRational p :: Double) :: Integer
