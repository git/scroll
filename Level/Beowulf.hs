module Level.Beowulf where

import System.Random
import Data.Char

import Types
import Rand
import Level.Random

level :: Rand -> Int -> Level
level r minheight = beowulfIntro ++ map trim randomness
  where
	randomness = formatLevel id minheight $
		addFeatures (zip stream features)
	stream = drop 1 $ concatMap words $
		withStdGen r (cycle reverseBeowulf)
			(infiniteStream reverseBeowulf)
	features = filter wanted $ featureStream r
	wanted ElipsesFeature = False
	wanted EOLFeature = False
	wanted _ = True

trim :: String -> String
trim = reverse . dropWhile isSpace . reverse

infiniteStream :: [String] -> StdGen -> [String]
infiniteStream l = concat . stream
  where
	stream g =
		let (n, g') = randomR range g
		in words (l !! n) : stream g'
	range = (0, length l - 1)

addpunct :: String -> String
addpunct [] = []
addpunct l = case reverse l of
	(c:_) | isPunctuation c -> l ++ "\n"
	_ -> l ++ " -- \n"

beowulfIntro :: [String]
beowulfIntro =
	[ "|||                                                  ||||||||||||"
	, "|| Lo! the Spear-Danes' glory                               ||"
	, "|                     through splendid achievements          ||"
	, "| The folk-kings' former fame we have heard of,               || "
	, "| How princes displayed then their prowess-in-battle.         ||"
	, "|||                                                          |||"
	, "| Oft Scyld the Scefing from scathers in numbers            ||||"
	]

reverseBeowulf :: [String]
reverseBeowulf = map (unwords . reverse . words) $ map (addpunct . trim) $ lines $ map (\c -> if c == '↓' then '\n' else c) $ "↓\
\ From many a people their mead-benches tore. ↓\
\ Since first he found him friendless and wretched, ↓\
\ The earl had had terror: comfort he got for it, ↓\
\ Waxed 'neath the welkin, world-honor gained, ↓\
\ Till all his neighbors o'er sea were compelled to ↓\
\ Bow to his bidding and bring him their tribute: ↓\
\ An excellent atheling! After was borne him ↓\
\ A son and heir, young in his dwelling, ↓\
\ Whom God-Father sent to solace the people. ↓\
\ He had marked the misery malice had caused them, ↓\
\ That reaved of their rulers they wretched had erstwhile ↓\
\ Long been afflicted. The Lord, in requital, ↓\
\ Wielder of Glory, with world-honor blessed him. ↓\
\ Famed was Beowulf, far spread the glory ↓\
\ Of Scyld's great son in the lands of the Danemen. ↓\
\ So the carle that is young, by kindnesses rendered ↓\
\ The friends of his father, with fees in abundance ↓\
\ Must be able to earn that when age approacheth ↓\
\ Eager companions aid him requitingly, ↓\
\ When war assaults him serve him as liegemen: ↓\
\ By praise-worthy actions must honor be got ↓\
\ 'Mong all of the races. At the hour that was fated ↓\
\ Scyld then departed to the All-Father's keeping ↓\
\ Warlike to wend him; away then they bare him ↓\
\ To the flood of the current, his fond-loving comrades, ↓\
\ As himself he had bidden, while the friend of the Scyldings ↓\
\ Word-sway wielded, and the well-loved land-prince ↓\
\ Long did rule them. The ring-stemmed vessel, ↓\
\ Bark of the atheling, lay there at anchor, ↓\
\ Icy in glimmer and eager for sailing; ↓\
\ The beloved leader laid they down there, ↓\
\ Giver of rings, on the breast of the vessel, ↓\
\ The famed by the mainmast. A many of jewels, ↓\
\ Of fretted embossings, from far-lands brought over, ↓\
\ Was placed near at hand then; and heard I not ever ↓\
\ That a folk ever furnished a float more superbly ↓\
\ With weapons of warfare, weeds for the battle, ↓\
\ Bills and burnies; on his bosom sparkled ↓\
\ Many a jewel that with him must travel ↓\
\ On the flush of the flood afar on the current. ↓\
\ And favors no fewer they furnished him soothly, ↓\
\ Excellent folk-gems, than others had given him ↓\
\ Who when first he was born outward did send him ↓\
\ Lone on the main, the merest of infants: ↓\
\ And a gold-fashioned standard they stretched under heaven ↓\
\ High o'er his head, let the holm-currents bear him, ↓\
\ Seaward consigned him: sad was their spirit, ↓\
\ Their mood very mournful. Men are not able ↓\
\ Soothly to tell us, they in halls who reside, ↓\
\ Heroes under heaven, to what haven he hied. ↓\
\  ↓\
\ In the boroughs then Beowulf, bairn of the Scyldings, ↓\
\ Beloved land-prince, for long-lasting season ↓\
\ Was famed mid the folk, his father departed, ↓\
\ The prince from his dwelling, till afterward sprang ↓\
\ Great-minded Healfdene; the Danes in his lifetime ↓\
\ He graciously governed, grim-mooded, aged. ↓\
\ Four bairns of his body born in succession ↓\
\ Woke in the world, war-troopers' leader ↓\
\ Heorogar, Hrothgar, and Halga the good; ↓\
\ Heard I that Elan was Ongentheow's consort, ↓\
\ The well-beloved bedmate of the War-Scylfing leader. ↓\
\ Then glory in battle to Hrothgar was given, ↓\
\ Waxing of war-fame, that willingly kinsmen ↓\
\ Obeyed his bidding, till the boys grew to manhood, ↓\
\ A numerous band. It burned in his spirit ↓\
\ To urge his folk to found a great building, ↓\
\ A mead-hall grander than men of the era ↓\
\ Ever had heard of, and in it to share ↓\
\ With young and old all of the blessings ↓\
\ The Lord had allowed him, save life and retainers. ↓\
\ Then the work I find afar was assigned ↓\
\ To many races in middle-earth's regions, ↓\
\ To adorn the great folk-hall. In due time it happened ↓\
\ Early 'mong men, that 'twas finished entirely, ↓\
\ The greatest of hall-buildings; Heorot he named it ↓\
\ Who wide-reaching word-sway wielded 'mong earlmen. ↓\
\ His promise he brake not, rings he lavished, ↓\
\ Treasure at banquet. Towered the hall up ↓\
\ High and horn-crested, huge between antlers: ↓\
\ It battle-waves bided, the blasting fire-demon; ↓\
\ Ere long then from hottest hatred must sword-wrath ↓\
\ Arise for a woman's husband and father. ↓\
\ Then the mighty war-spirit endured for a season, ↓\
\ Bore it bitterly, he who bided in darkness, ↓\
\ That light-hearted laughter loud in the building ↓\
\ Greeted him daily; there was dulcet harp-music, ↓\
\ Clear song of the singer. He said that was able ↓\
\ To tell from of old earthmen's beginnings, ↓\
\ That Father Almighty earth had created, ↓\
\ The winsome wold that the water encircleth, ↓\
\ Set exultingly the sun's and the moon's beams ↓\
\ To lavish their lustre on land-folk and races, ↓\
\ And earth He embellished in all her regions ↓\
\ With limbs and leaves; life He bestowed too ↓\
\ On all the kindreds that live under heaven. ↓\
\ So blessed with abundance, brimming with joyance, ↓\
\ The warriors abided, till a certain one gan to ↓\
\ Dog them with deeds of direfullest malice, ↓\
\ A foe in the hall-building: this horrible stranger ↓\
\ Was Grendel entitled, the march-stepper famous ↓\
\ Who dwelt in the moor-fens, the marsh and the fastness; ↓\
\ The wan-mooded being abode for a season ↓\
\ In the land of the giants, when the Lord and Creator ↓\
\ Had banned him and branded. For that bitter murder, ↓\
\ The killing of Abel, all-ruling Father ↓\
\ The kindred of Cain crushed with His vengeance; ↓\
\ In the feud He rejoiced not, but far away drove him ↓\
\ From kindred and kind, that crime to atone for, ↓\
\ Meter of Justice. Thence ill-favored creatures, ↓\
\ Elves and giants, monsters of ocean, ↓\
\ Came into being, and the giants that longtime ↓\
\ Grappled with God; He gave them requital. ↓\
\  ↓\
\ When the sun was sunken, he set out to visit ↓\
\ The lofty hall-building, how the Ring-Danes had used it ↓\
\ For beds and benches when the banquet was over. ↓\
\ Then he found there reposing many a noble ↓\
\ Asleep after supper; sorrow the heroes, ↓\
\ Misery knew not. The monster of evil ↓\
\ Greedy and cruel tarried but little, ↓\
\ Fell and frantic, and forced from their slumbers ↓\
\ Thirty of thanemen; thence he departed ↓\
\ Leaping and laughing, his lair to return to, ↓\
\ With surfeit of slaughter sallying homeward. ↓\
\ In the dusk of the dawning, as the day was just breaking, ↓\
\ Was Grendel's prowess revealed to the warriors: ↓\
\ Then, his meal-taking finished, a moan was uplifted, ↓\
\ Morning-cry mighty. The man-ruler famous, ↓\
\ The long-worthy atheling, sat very woful, ↓\
\ Suffered great sorrow, sighed for his liegemen, ↓\
\ When they had seen the track of the hateful pursuer, ↓\
\ The spirit accursed: too crushing that sorrow, ↓\
\ Too loathsome and lasting. Not longer he tarried, ↓\
\ But one night after continued his slaughter ↓\
\ Shameless and shocking, shrinking but little ↓\
\ From malice and murder; they mastered him fully. ↓\
\ He was easy to find then who otherwhere looked for ↓\
\ A pleasanter place of repose in the lodges, ↓\
\ A bed in the bowers. Then was brought to his notice ↓\
\ Told him truly by token apparent ↓\
\ The hall-thane's hatred: he held himself after ↓\
\ Further and faster who the foeman did baffle. ↓\
\ So ruled he and strongly strove against justice ↓\
\ Lone against all men, till empty uptowered ↓\
\ The choicest of houses. Long was the season: ↓\
\ Twelve-winters' time torture suffered ↓\
\ The friend of the Scyldings, every affliction, ↓\
\ Endless agony; hence it after became ↓\
\ Certainly known to the children of men ↓\
\ Sadly in measures, that long against Hrothgar ↓\
\ Grendel struggled:--his grudges he cherished, ↓\
\ Murderous malice, many a winter, ↓\
\ Strife unremitting, and peacefully wished he ↓\
\ Life-woe to lift from no liegeman at all of ↓\
\ The men of the Dane-folk, for money to settle, ↓\
\ No counsellor needed count for a moment ↓\
\ On handsome amends at the hands of the murderer; ↓\
\ The monster of evil fiercely did harass, ↓\
\ The ill-planning death-shade, both elder and younger, ↓\
\ Trapping and tricking them. He trod every night then ↓\
\ The mist-covered moor-fens; men do not know where ↓\
\ Witches and wizards wander and ramble. ↓\
\ So the foe of mankind many of evils ↓\
\ Grievous injuries, often accomplished, ↓\
\ Horrible hermit; Heort he frequented, ↓\
\ Gem-bedecked palace, when night-shades had fallen ↓\
\ Since God did oppose him, not the throne could he touch, ↓\
\ The light-flashing jewel, love of Him knew not. ↓\
\ 'Twas a fearful affliction to the friend of the Scyldings ↓\
\ Soul-crushing sorrow. Not seldom in private ↓\
\ Sat the king in his council; conference held they ↓\
\ What the braves should determine 'gainst terrors unlooked for. ↓\
\ At the shrines of their idols often they promised ↓\
\ Gifts and offerings, earnestly prayed they ↓\
\ The devil from hell would help them to lighten ↓\
\ Their people's oppression. Such practice they used then, ↓\
\ Hope of the heathen; hell they remembered ↓\
\ In innermost spirit, God they knew not, ↓\
\ Judge of their actions, All-wielding Ruler, ↓\
\ No praise could they give the Guardian of Heaven, ↓\
\ The Wielder of Glory. Woe will be his who ↓\
\ Through furious hatred his spirit shall drive to ↓\
\ The clutch of the fire, no comfort shall look for, ↓\
\ Wax no wiser; well for the man who, ↓\
\ Living his life-days, his Lord may face ↓\
\ And find defence in his Father's embrace! ↓\
\  ↓\
\ So Healfdene's kinsman constantly mused on ↓\
\ His long-lasting sorrow; the battle-thane clever ↓\
\ Was not anywise able evils to 'scape from: ↓\
\ Too crushing the sorrow that came to the people, ↓\
\ Loathsome and lasting the life-grinding torture, ↓\
\ Greatest of night-woes. So Higelac's liegeman, ↓\
\ Good amid Geatmen, of Grendel's achievements ↓\
\ Heard in his home: of heroes then living ↓\
\ He was stoutest and strongest, sturdy and noble. ↓\
\ He bade them prepare him a bark that was trusty; ↓\
\ He said he the war-king would seek o'er the ocean, ↓\
\ The folk-leader noble, since he needed retainers. ↓\
\ For the perilous project prudent companions ↓\
\ Chided him little, though loving him dearly; ↓\
\ They egged the brave atheling, augured him glory. ↓\
\  ↓\
\ The excellent knight from the folk of the Geatmen ↓\
\ Had liegemen selected, likest to prove them ↓\
\ Trustworthy warriors; with fourteen companions ↓\
\ The vessel he looked for; a liegeman then showed them, ↓\
\ A sea-crafty man, the bounds of the country. ↓\
\ Fast the days fleeted; the float was a-water, ↓\
\ The craft by the cliff. Clomb to the prow then ↓\
\ Well-equipped warriors: the wave-currents twisted ↓\
\ The sea on the sand; soldiers then carried ↓\
\ On the breast of the vessel bright-shining jewels, ↓\
\ Handsome war-armor; heroes outshoved then, ↓\
\ Warmen the wood-ship, on its wished-for adventure. ↓\
\  ↓\
\ The foamy-necked floater fanned by the breeze, ↓\
\ Likest a bird, glided the waters, ↓\
\  ↓\
\ Till twenty and four hours thereafter ↓\
\ The twist-stemmed vessel had traveled such distance ↓\
\ That the sailing-men saw the sloping embankments, ↓\
\ The sea cliffs gleaming, precipitous mountains, ↓\
\ Nesses enormous: they were nearing the limits ↓\
\ At the end of the ocean. Up thence quickly ↓\
\ The men of the Weders clomb to the mainland, ↓\
\ Fastened their vessel, battle weeds rattled, ↓\
\ War burnies clattered, the Wielder they thanked ↓\
\ That the ways o'er the waters had waxen so gentle. ↓\
\  ↓\
\ Then well from the cliff edge the guard of the Scyldings ↓\
\ Who the sea-cliffs should see to, saw o'er the gangway ↓\
\ Brave ones bearing beauteous targets, ↓\
\ Armor all ready, anxiously thought he, ↓\
\ Musing and wondering what men were approaching. ↓\
\ High on his horse then Hrothgar's retainer ↓\
\ Turned him to coastward, mightily brandished ↓\
\ His lance in his hands, questioned with boldness. ↓\
\ Who are ye men here, mail-covered warriors ↓\
\ Clad in your corslets, come thus a-driving ↓\
\ A high riding ship o'er the shoals of the waters, ↓\
\ And hither 'neath helmets have hied o'er the ocean? ↓\
\ I have been strand-guard, standing as warden, ↓\
\ Lest enemies ever anywise ravage ↓\
\ Danish dominions with army of war-ships. ↓\
\ More boldly never have warriors ventured ↓\
\ Hither to come; of kinsmen's approval, ↓\
\ Word-leave of warriors, I ween that ye surely ↓\
\  ↓\
\ Nothing have known. Never a greater one ↓\
\ Of earls o'er the earth have _I_ had a sight of ↓\
\ Than is one of your number, a hero in armor; ↓\
\ No low-ranking fellow adorned with his weapons, ↓\
\ But launching them little, unless looks are deceiving, ↓\
\ And striking appearance. Ere ye pass on your journey ↓\
\ As treacherous spies to the land of the Scyldings ↓\
\ And farther fare, I fully must know now ↓\
\ What race ye belong to. Ye far-away dwellers, ↓\
\ Sea-faring sailors, my simple opinion ↓\
\ Hear ye and hearken: haste is most fitting ↓\
\ Plainly to tell me what place ye are come from. ↓\
\  ↓\
\ We are sprung from the lineage of the people of Geatland, ↓\
\ And Higelac's hearth-friends. To heroes unnumbered ↓\
\  ↓\
\ My father was known, a noble head-warrior ↓\
\ Ecgtheow titled; many a winter ↓\
\ He lived with the people, ere he passed on his journey, ↓\
\ Old from his dwelling; each of the counsellors ↓\
\ Widely mid world-folk well remembers him. ↓\
\  ↓\
\ We, kindly of spirit, the lord of thy people, ↓\
\ The son of King Healfdene, have come here to visit, ↓\
\ Folk-troop's defender: be free in thy counsels! ↓\
\ To the noble one bear we a weighty commission, ↓\
\ The helm of the Danemen; we shall hide, I ween, ↓\
\  ↓\
\ Naught of our message. Thou know'st if it happen, ↓\
\ As we soothly heard say, that some savage despoiler, ↓\
\ Some hidden pursuer, on nights that are murky ↓\
\ By deeds very direful 'mid the Danemen exhibits ↓\
\ Hatred unheard of, horrid destruction ↓\
\ And the falling of dead. From feelings least selfish ↓\
\  ↓\
\ I am able to render counsel to Hrothgar, ↓\
\ How he, wise and worthy, may worst the destroyer, ↓\
\ If the anguish of sorrow should ever be lessened, ↓\
\ Comfort come to him, and care-waves grow cooler, ↓\
\ Or ever hereafter he agony suffer ↓\
\ And troublous distress, while towereth upward ↓\
\ The handsomest of houses high on the summit. ↓\
\  ↓\
\ Bestriding his stallion, the strand-watchman answered, ↓\
\ The doughty retainer: The difference surely ↓\
\ 'Twixt words and works, the warlike shield-bearer ↓\
\ Who judgeth wisely well shall determine. ↓\
\ This band, I hear, beareth no malice ↓\
\  ↓\
\ To the prince of the Scyldings. Pass ye then onward ↓\
\ With weapons and armor. I shall lead you in person; ↓\
\ To my war-trusty vassals command I shall issue ↓\
\ To keep from all injury your excellent vessel, ↓\
\  ↓\
\ Your fresh-tarred craft, 'gainst every opposer ↓\
\ Close by the sea-shore, till the curved-necked bark shall ↓\
\ Waft back again the well-beloved hero ↓\
\ O'er the way of the water to Weder dominions. ↓\
\  ↓\
\ To warrior so great 'twill be granted sure ↓\
\ In the storm of strife to stand secure. ↓\
\ Onward they fared then, the vessel lay quiet, ↓\
\ The broad-bosomed bark was bound by its cable, ↓\
\ Firmly at anchor; the boar-signs glistened ↓\
\ Bright on the visors vivid with gilding, ↓\
\ Blaze-hardened, brilliant; the boar acted warden. ↓\
\ The heroes hastened, hurried the liegemen, ↓\
\  ↓\
\ Descended together, till they saw the great palace, ↓\
\ The well-fashioned wassail-hall wondrous and gleaming: ↓\
\  ↓\
\ 'Mid world-folk and kindreds that was widest reputed ↓\
\ Of halls under heaven which the hero abode in; ↓\
\ Its lustre enlightened lands without number. ↓\
\ Then the battle-brave hero showed them the glittering ↓\
\ Court of the bold ones, that they easily thither ↓\
\ Might fare on their journey; the aforementioned warrior ↓\
\ Turning his courser, quoth as he left them: ↓\
\  ↓\
\ 'Tis time I were faring; Father Almighty ↓\
\ Grant you His grace, and give you to journey ↓\
\ Safe on your mission! To the sea I will get me ↓\
\ 'Gainst hostile warriors as warden to stand. ↓\
\  ↓\
\ The highway glistened with many-hued pebble, ↓\
\ A by-path led the liegemen together. ↓\
\ Firm and hand-locked the war-burnie glistened, ↓\
\ The ring-sword radiant rang 'mid the armor ↓\
\ As the party was approaching the palace together ↓\
\  ↓\
\ In warlike equipments. 'Gainst the wall of the building ↓\
\ Their wide-fashioned war-shields they weary did set then, ↓\
\ Battle-shields sturdy; benchward they turned then; ↓\
\ Their battle-sarks rattled, the gear of the heroes; ↓\
\ The lances stood up then, all in a cluster, ↓\
\ The arms of the seamen, ashen-shafts mounted ↓\
\ With edges of iron: the armor-clad troopers ↓\
\  ↓\
\ Were decked with weapons. Then a proud-mooded hero ↓\
\ Asked of the champions questions of lineage: ↓\
\ From what borders bear ye your battle-shields plated, ↓\
\ Gilded and gleaming, your gray-colored burnies, ↓\
\ Helmets with visors and heap of war-lances?-- ↓\
\ To Hrothgar the king I am servant and liegeman. ↓\
\ 'Mong folk from far-lands found I have never ↓\
\  ↓\
\ Men so many of mien more courageous. ↓\
\ I ween that from valor, nowise as outlaws, ↓\
\ But from greatness of soul ye sought for King Hrothgar. ↓\
\  ↓\
\ Then the strength-famous earlman answer rendered, ↓\
\ The proud-mooded Wederchief replied to his question, ↓\
\  ↓\
\ Hardy 'neath helmet: Higelac's mates are we; ↓\
\ Beowulf hight I. To the bairn of Healfdene, ↓\
\ The famous folk-leader, I freely will tell ↓\
\ To thy prince my commission, if pleasantly hearing ↓\
\ He'll grant we may greet him so gracious to all men. ↓\
\ Wulfgar replied then, he was prince of the Wendels, ↓\
\ His boldness of spirit was known unto many, ↓\
\ His prowess and prudence: The prince of the Scyldings, ↓\
\  ↓\
\ The friend-lord of Danemen, I will ask of thy journey, ↓\
\ The giver of rings, as thou urgest me do it, ↓\
\ The folk-chief famous, and inform thee early ↓\
\ What answer the good one mindeth to render me. ↓\
\ He turned then hurriedly where Hrothgar was sitting, ↓\
\ Old and hoary, his earlmen attending him; ↓\
\ The strength-famous went till he stood at the shoulder ↓\
\ Of the lord of the Danemen, of courteous thanemen ↓\
\ The custom he minded. Wulfgar addressed then ↓\
\ His friendly liegelord: Folk of the Geatmen ↓\
\  ↓\
\ O'er the way of the waters are wafted hither, ↓\
\ Faring from far-lands: the foremost in rank ↓\
\ The battle-champions Beowulf title. ↓\
\ They make this petition: with thee, O my chieftain, ↓\
\ To be granted a conference; O gracious King Hrothgar, ↓\
\ Friendly answer refuse not to give them! ↓\
\  ↓\
\ In war-trappings weeded worthy they seem ↓\
\ Of earls to be honored; sure the atheling is doughty ↓\
\ Who headed the heroes hitherward coming. ↓\
\  ↓\
\ Hrothgar answered, helm of the Scyldings: ↓\
\ I remember this man as the merest of striplings. ↓\
\ His father long dead now was Ecgtheow titled, ↓\
\ Him Hrethel the Geatman granted at home his ↓\
\ One only daughter; his battle-brave son ↓\
\ Is come but now, sought a trustworthy friend. ↓\
\ Seafaring sailors asserted it then, ↓\
\  ↓\
\ Who valuable gift-gems of the Geatmen carried ↓\
\ As peace-offering thither, that he thirty men's grapple ↓\
\ Has in his hand, the hero-in-battle. ↓\
\  ↓\
\ The holy Creator usward sent him, ↓\
\ To West-Dane warriors, I ween, for to render ↓\
\ 'Gainst Grendel's grimness gracious assistance: ↓\
\ I shall give to the good one gift-gems for courage. ↓\
\ Hasten to bid them hither to speed them, ↓\
\ To see assembled this circle of kinsmen; ↓\
\ Tell them expressly they're welcome in sooth to ↓\
\ The men of the Danes. To the door of the building ↓\
\  ↓\
\ Wulfgar went then, this word-message shouted: ↓\
\ My victorious liegelord bade me to tell you, ↓\
\ The East-Danes' atheling, that your origin knows he, ↓\
\ And o'er wave-billows wafted ye welcome are hither, ↓\
\ Valiant of spirit. Ye straightway may enter ↓\
\ Clad in corslets, cased in your helmets, ↓\
\ To see King Hrothgar. Here let your battle-boards, ↓\
\ Wood-spears and war-shafts, await your conferring. ↓\
\ The mighty one rose then, with many a liegeman, ↓\
\ An excellent thane-group; some there did await them, ↓\
\ And as bid of the brave one the battle-gear guarded. ↓\
\ Together they hied them, while the hero did guide them, ↓\
\ 'Neath Heorot's roof; the high-minded went then ↓\
\ Sturdy 'neath helmet till he stood in the building. ↓\
\ Beowulf spake, his burnie did glisten, ↓\
\ His armor seamed over by the art of the craftsman: ↓\
\  ↓\
\ Hail thou, Hrothgar! I am Higelac's kinsman ↓\
\ And vassal forsooth; many a wonder ↓\
\ I dared as a stripling. The doings of Grendel, ↓\
\ In far-off fatherland I fully did know of: ↓\
\ Sea-farers tell us, this hall-building standeth, ↓\
\ Excellent edifice, empty and useless ↓\
\ To all the earlmen after evenlight's glimmer ↓\
\ 'Neath heaven's bright hues hath hidden its glory. ↓\
\ This my earls then urged me, the most excellent of them, ↓\
\ Carles very clever, to come and assist thee, ↓\
\ Folk-leader Hrothgar; fully they knew of ↓\
\  ↓\
\ The strength of my body. Themselves they beheld me ↓\
\ When I came from the contest, when covered with gore ↓\
\ Foes I escaped from, where five I had bound, ↓\
\ The giant-race wasted, in the waters destroying ↓\
\ The nickers by night, bore numberless sorrows, ↓\
\ The Weders avenged, woes had they suffered ↓\
\ Enemies ravaged; alone now with Grendel ↓\
\  ↓\
\ I shall manage the matter, with the monster of evil, ↓\
\ The giant, decide it. Thee I would therefore ↓\
\ Beg of thy bounty, Bright-Danish chieftain, ↓\
\ Lord of the Scyldings, this single petition: ↓\
\ Not to refuse me, defender of warriors, ↓\
\ Friend-lord of folks, so far have I sought thee, ↓\
\ That _I_ may unaided, my earlmen assisting me, ↓\
\ This brave-mooded war-band, purify Heorot. ↓\
\ I have heard on inquiry, the horrible creature ↓\
\  ↓\
\ From veriest rashness recks not for weapons; ↓\
\ I this do scorn then, so be Higelac gracious, ↓\
\ My liegelord beloved, lenient of spirit, ↓\
\ To bear a blade or a broad-fashioned target, ↓\
\ A shield to the onset; only with hand-grip ↓\
\  ↓\
\ The foe I must grapple, fight for my life then, ↓\
\ Foeman with foeman; he fain must rely on ↓\
\ The doom of the Lord whom death layeth hold of. ↓\
\  ↓\
\ I ween he will wish, if he win in the struggle, ↓\
\ To eat in the war-hall earls of the Geat-folk, ↓\
\ Boldly to swallow them, as of yore he did often ↓\
\ The best of the Hrethmen! Thou needest not trouble ↓\
\ A head-watch to give me; he will have me dripping ↓\
\  ↓\
\ And dreary with gore, if death overtake me, ↓\
\ Will bear me off bleeding, biting and mouthing me, ↓\
\ The hermit will eat me, heedless of pity, ↓\
\ Marking the moor-fens; no more wilt thou need then ↓\
\  ↓\
\ Find me my food. If I fall in the battle, ↓\
\ Send to Higelac the armor that serveth ↓\
\ To shield my bosom, the best of equipments, ↓\
\ Richest of ring-mails; 'tis the relic of Hrethla, ↓\
\  ↓\
\ The work of Wayland. Goes Weird as she must go! ↓\
\  ↓\
\ Hrothgar discoursed, helm of the Scyldings: ↓\
\ To defend our folk and to furnish assistance, ↓\
\ Thou soughtest us hither, good friend Beowulf. ↓\
\  ↓\
\ The fiercest of feuds thy father engaged in, ↓\
\ Heatholaf killed he in hand-to-hand conflict ↓\
\ 'Mid Wilfingish warriors; then the Wederish people ↓\
\ For fear of a feud were forced to disown him. ↓\
\ Thence flying he fled to the folk of the South-Danes, ↓\
\ The race of the Scyldings, o'er the roll of the waters; ↓\
\ I had lately begun then to govern the Danemen, ↓\
\ The hoard-seat of heroes held in my youth, ↓\
\ Rich in its jewels: dead was Heregar, ↓\
\ My kinsman and elder had earth-joys forsaken, ↓\
\ Healfdene his bairn. He was better than I am! ↓\
\ That feud thereafter for a fee I compounded; ↓\
\ O'er the weltering waters to the Wilfings I sent ↓\
\ Ornaments old; oaths did he swear me. ↓\
\  ↓\
\ It pains me in spirit to any to tell it, ↓\
\ What grief in Heorot Grendel hath caused me, ↓\
\ What horror unlooked-for, by hatred unceasing. ↓\
\ Waned is my war-band, wasted my hall-troop; ↓\
\ Weird hath offcast them to the clutches of Grendel. ↓\
\ God can easily hinder the scather ↓\
\ From deeds so direful. Oft drunken with beer ↓\
\  ↓\
\ O'er the ale-vessel promised warriors in armor ↓\
\ They would willingly wait on the wassailing-benches ↓\
\ A grapple with Grendel, with grimmest of edges! ↓\
\ Then this mead-hall at morning with murder was reeking, ↓\
\ The building was bloody at breaking of daylight, ↓\
\ The bench-deals all flooded, dripping and bloodied, ↓\
\ The folk-hall was gory: I had fewer retainers, ↓\
\ Dear-beloved warriors, whom death had laid hold of. ↓\
\  ↓\
\ Sit at the feast now, thy intents unto heroes, ↓\
\ Thy victor-fame show, as thy spirit doth urge thee! ↓\
\  ↓\
\ For the men of the Geats then together assembled, ↓\
\ In the beer-hall blithesome a bench was made ready; ↓\
\ There warlike in spirit they went to be seated, ↓\
\ Proud and exultant. A liegeman did service, ↓\
\ Who a beaker embellished bore with decorum, ↓\
\  ↓\
\ And gleaming-drink poured. The gleeman sang whilom ↓\
\  ↓\
\ Hearty in Heorot; there was heroes' rejoicing, ↓\
\ A numerous war-band of Weders and Danemen. ↓\
\ Unferth spoke up, Ecglaf his son, ↓\
\ Who sat at the feet of the lord of the Scyldings, ↓\
\ Opened the jousting, the journey of Beowulf, ↓\
\ Sea-farer doughty, gave sorrow to Unferth ↓\
\ And greatest chagrin, too, for granted he never ↓\
\ That any man else on earth should attain to, ↓\
\ Gain under heaven, more glory than he: ↓\
\  ↓\
\ Art thou that Beowulf with Breca did struggle, ↓\
\ On the wide sea-currents at swimming contended, ↓\
\ Where to humor your pride the ocean ye tried, ↓\
\  ↓\
\ From vainest vaunting adventured your bodies ↓\
\ In care of the waters? And no one was able ↓\
\ Nor lief nor loth one, in the least to dissuade you ↓\
\ Your difficult voyage; then ye ventured a-swimming, ↓\
\ Where your arms outstretching the streams ye did cover, ↓\
\ The mere-ways measured, mixing and stirring them, ↓\
\ Glided the ocean; angry the waves were, ↓\
\ With the weltering of winter. In the water's possession, ↓\
\ Ye toiled for a seven-night; he at swimming outdid thee, ↓\
\ In strength excelled thee. Then early at morning ↓\
\ On the Heathoremes' shore the holm-currents tossed him, ↓\
\ Sought he thenceward the home of his fathers, ↓\
\ Beloved of his liegemen, the land of the Brondings, ↓\
\  ↓\
\ The son of Beanstan hath soothly accomplished. ↓\
\ Then I ween thou wilt find thee less fortunate issue, ↓\
\  ↓\
\ Though ever triumphant in onset of battle, ↓\
\ A grim grappling, if Grendel thou darest ↓\
\ For the space of a night near-by to wait for! ↓\
\  ↓\
\ Beowulf answered, offspring of Ecgtheow: ↓\
\ My good friend Unferth, sure freely and wildly, ↓\
\  ↓\
\ Thou fuddled with beer of Breca hast spoken, ↓\
\ Hast told of his journey! A fact I allege it, ↓\
\ That greater strength in the waters I had then, ↓\
\ Ills in the ocean, than any man else had. ↓\
\ We made agreement as the merest of striplings ↓\
\ Out on the ocean; it all we accomplished. ↓\
\ While swimming the sea-floods, sword-blade unscabbarded ↓\
\ Boldly we brandished, our bodies expected ↓\
\ To shield from the sharks. He sure was unable ↓\
\  ↓\
\ To swim on the waters further than I could, ↓\
\ More swift on the waves, nor _would_ I from him go. ↓\
\ Then we two companions stayed in the ocean ↓\
\  ↓\
\ Five nights together, till the currents did part us, ↓\
\ The weltering waters, weathers the bleakest, ↓\
\ And nethermost night, and the north-wind whistled ↓\
\ Fierce in our faces; fell were the billows. ↓\
\ The mere fishes' mood was mightily ruffled: ↓\
\ And there against foemen my firm-knotted corslet, ↓\
\ Hand-jointed, hardy, help did afford me; ↓\
\ My battle-sark braided, brilliantly gilded, ↓\
\ Lay on my bosom. To the bottom then dragged me, ↓\
\ A hateful fiend-scather, seized me and held me, ↓\
\ Grim in his grapple: 'twas granted me, nathless, ↓\
\ To pierce the monster with the point of my weapon, ↓\
\ My obedient blade; battle offcarried ↓\
\ The mighty mere-creature by means of my hand-blow. ↓\
\  ↓\
\ So ill-meaning enemies often did cause me ↓\
\ Sorrow the sorest. I served them, in quittance, ↓\
\  ↓\
\ With my dear-loved sword, as in sooth it was fitting; ↓\
\ They missed the pleasure of feasting abundantly, ↓\
\ Ill-doers evil, of eating my body, ↓\
\ Of surrounding the banquet deep in the ocean; ↓\
\ But wounded with edges early at morning ↓\
\ They were stretched a-high on the strand of the ocean, ↓\
\  ↓\
\ Put to sleep with the sword, that sea-going travelers ↓\
\ No longer thereafter were hindered from sailing ↓\
\ The foam-dashing currents. Came a light from the east, ↓\
\ God's beautiful beacon; the billows subsided, ↓\
\ That well I could see the nesses projecting, ↓\
\  ↓\
\ The blustering crags. Weird often saveth ↓\
\ The undoomed hero if doughty his valor! ↓\
\ But me did it fortune to fell with my weapon ↓\
\ Nine of the nickers. Of night-struggle harder ↓\
\ 'Neath dome of the heaven heard I but rarely, ↓\
\ Nor of wight more woful in the waves of the ocean; ↓\
\ Yet I 'scaped with my life the grip of the monsters, ↓\
\  ↓\
\ Weary from travel. Then the waters bare me ↓\
\ To the land of the Finns, the flood with the current, ↓\
\  ↓\
\ The weltering waves. Not a word hath been told me ↓\
\ Of deeds so daring done by thee, Unferth, ↓\
\ And of sword-terror none; never hath Breca ↓\
\ At the play of the battle, nor either of you two, ↓\
\ Feat so fearless performed with weapons ↓\
\ Glinting and gleaming . . . . . . . . . . . . ↓\
\ . . . . . . . . . . . . I utter no boasting; ↓\
\  ↓\
\ Though with cold-blooded cruelty thou killedst thy brothers, ↓\
\ Thy nearest of kin; thou needs must in hell get ↓\
\ Direful damnation, though doughty thy wisdom. ↓\
\ I tell thee in earnest, offspring of Ecglaf, ↓\
\ Never had Grendel such numberless horrors, ↓\
\ The direful demon, done to thy liegelord, ↓\
\ Harrying in Heorot, if thy heart were as sturdy, ↓\
\  ↓\
\ Thy mood as ferocious as thou dost describe them. ↓\
\ He hath found out fully that the fierce-burning hatred, ↓\
\ The edge-battle eager, of all of your kindred, ↓\
\ Of the Victory-Scyldings, need little dismay him: ↓\
\ Oaths he exacteth, not any he spares ↓\
\  ↓\
\ Of the folk of the Danemen, but fighteth with pleasure, ↓\
\ Killeth and feasteth, no contest expecteth ↓\
\  ↓\
\ From Spear-Danish people. But the prowess and valor ↓\
\ Of the earls of the Geatmen early shall venture ↓\
\ To give him a grapple. He shall go who is able ↓\
\ Bravely to banquet, when the bright-light of morning ↓\
\  ↓\
\ Which the second day bringeth, the sun in its ether-robes, ↓\
\ O'er children of men shines from the southward! ↓\
\ Then the gray-haired, war-famed giver of treasure ↓\
\  ↓\
\ Was blithesome and joyous, the Bright-Danish ruler ↓\
\ Expected assistance; the people's protector ↓\
\  ↓\
\ Heard from Beowulf his bold resolution. ↓\
\ There was laughter of heroes; loud was the clatter, ↓\
\ The words were winsome. Wealhtheow advanced then, ↓\
\  ↓\
\ Consort of Hrothgar, of courtesy mindful, ↓\
\ Gold-decked saluted the men in the building, ↓\
\ And the freeborn woman the beaker presented ↓\
\  ↓\
\ To the lord of the kingdom, first of the East-Danes, ↓\
\ Bade him be blithesome when beer was a-flowing, ↓\
\ Lief to his liegemen; he lustily tasted ↓\
\ Of banquet and beaker, battle-famed ruler. ↓\
\ The Helmingish lady then graciously circled ↓\
\ 'Mid all the liegemen lesser and greater: ↓\
\  ↓\
\ Treasure-cups tendered, till time was afforded ↓\
\ That the decorous-mooded, diademed folk-queen ↓\
\  ↓\
\ Might bear to Beowulf the bumper o'errunning; ↓\
\ She greeted the Geat-prince, God she did thank, ↓\
\ Most wise in her words, that her wish was accomplished, ↓\
\ That in any of earlmen she ever should look for ↓\
\ Solace in sorrow. He accepted the beaker, ↓\
\ Battle-bold warrior, at Wealhtheow's giving, ↓\
\  ↓\
\ Then equipped for combat quoth he in measures, ↓\
\ Beowulf spake, offspring of Ecgtheow: ↓\
\ I purposed in spirit when I mounted the ocean, ↓\
\  ↓\
\ When I boarded my boat with a band of my liegemen, ↓\
\ I would work to the fullest the will of your people ↓\
\ Or in foe's-clutches fastened fall in the battle. ↓\
\ Deeds I shall do of daring and prowess, ↓\
\ Or the last of my life-days live in this mead-hall. ↓\
\ These words to the lady were welcome and pleasing, ↓\
\ The boast of the Geatman; with gold trappings broidered ↓\
\ Went the freeborn folk-queen her fond-lord to sit by. ↓\
\  ↓\
\ Then again as of yore was heard in the building ↓\
\ Courtly discussion, conquerors' shouting, ↓\
\ Heroes were happy, till Healfdene's son would ↓\
\ Go to his slumber to seek for refreshing; ↓\
\ For the horrid hell-monster in the hall-building knew he ↓\
\ A fight was determined, since the light of the sun they ↓\
\ No longer could see, and lowering darkness ↓\
\ O'er all had descended, and dark under heaven ↓\
\ Shadowy shapes came shying around them. ↓\
\  ↓\
\ The liegemen all rose then. One saluted the other, ↓\
\ Hrothgar Beowulf, in rhythmical measures, ↓\
\ Wishing him well, and, the wassail-hall giving ↓\
\ To his care and keeping, quoth he departing: ↓\
\ Not to any one else have I ever entrusted, ↓\
\ But thee and thee only, the hall of the Danemen, ↓\
\ Since high I could heave my hand and my buckler. ↓\
\ Take thou in charge now the noblest of houses; ↓\
\ Be mindful of honor, exhibiting prowess, ↓\
\ Watch 'gainst the foeman! Thou shalt want no enjoyments, ↓\
\ Survive thou safely adventure so glorious! ↓\
\ Then Hrothgar departed, his earl-throng attending him, ↓\
\ Folk-lord of Scyldings, forth from the building; ↓\
\ The war-chieftain wished then Wealhtheow to look for, ↓\
\ The queen for a bedmate! To keep away Grendel ↓\
\  ↓\
\ The Glory of Kings had given a hall-watch, ↓\
\ As men heard recounted: for the king of the Danemen ↓\
\ He did special service, gave the giant a watcher: ↓\
\ And the prince of the Geatmen implicitly trusted ↓\
\  ↓\
\ His warlike strength and the Wielder's protection. ↓\
\  ↓\
\ His armor of iron off him he did then, ↓\
\ His helmet from his head, to his henchman committed ↓\
\ His chased-handled chain-sword, choicest of weapons, ↓\
\ And bade him bide with his battle-equipments. ↓\
\ The good one then uttered words of defiance, ↓\
\ Beowulf Geatman, ere his bed he upmounted: ↓\
\  ↓\
\ I hold me no meaner in matters of prowess, ↓\
\ In warlike achievements, than Grendel does himself; ↓\
\ Hence I seek not with sword-edge to sooth him to slumber, ↓\
\ Of life to bereave him, though well I am able. ↓\
\  ↓\
\ No battle-skill has he, that blows he should strike me, ↓\
\ To shatter my shield, though sure he is mighty ↓\
\ In strife and destruction; but struggling by night we ↓\
\ Shall do without edges, dare he to look for ↓\
\ Weaponless warfare, and wise-mooded Father ↓\
\ The glory apportion, God ever-holy, ↓\
\  ↓\
\ On which hand soever to him seemeth proper. ↓\
\ Then the brave-mooded hero bent to his slumber, ↓\
\ The pillow received the cheek of the noble; ↓\
\  ↓\
\ And many a martial mere-thane attending ↓\
\ Sank to his slumber. Seemed it unlikely ↓\
\  ↓\
\ That ever thereafter any should hope to ↓\
\ Be happy at home, hero-friends visit ↓\
\ Or the lordly troop-castle where he lived from his childhood; ↓\
\ They had heard how slaughter had snatched from the wine-hall, ↓\
\ Had recently ravished, of the race of the Scyldings ↓\
\  ↓\
\ Too many by far. But the Lord to them granted ↓\
\ The weaving of war-speed, to Wederish heroes ↓\
\ Aid and comfort, that every opponent ↓\
\ By one man's war-might they worsted and vanquished, ↓\
\  ↓\
\ By the might of himself; the truth is established ↓\
\ That God Almighty hath governed for ages ↓\
\ Kindreds and nations. A night very lurid ↓\
\  ↓\
\ The trav'ler-at-twilight came tramping and striding. ↓\
\ The warriors were sleeping who should watch the horned-building, ↓\
\  ↓\
\ One only excepted. 'Mid earthmen 'twas 'stablished, ↓\
\ Th' implacable foeman was powerless to hurl them ↓\
\ To the land of shadows, if the Lord were unwilling; ↓\
\ But serving as warder, in terror to foemen, ↓\
\ He angrily bided the issue of battle. ↓\
\  ↓\
\ 'Neath the cloudy cliffs came from the moor then ↓\
\ Grendel going, God's anger bare he! ↓\
\ The monster intended some one of earthmen ↓\
\ In the hall-building grand to entrap and make way with: ↓\
\  ↓\
\ He went under welkin where well he knew of ↓\
\ The wine-joyous building, brilliant with plating, ↓\
\ Gold-hall of earthmen. Not the earliest occasion ↓\
\  ↓\
\ He the home and manor of Hrothgar had sought: ↓\
\ Ne'er found he in life-days later nor earlier ↓\
\ Hardier hero, hall-thanes more sturdy! ↓\
\ Then came to the building the warrior marching, ↓\
\  ↓\
\ Bereft of his joyance. The door quickly opened ↓\
\ On fire-hinges fastened, when his fingers had touched it; ↓\
\ The fell one had flung then--his fury so bitter-- ↓\
\ Open the entrance. Early thereafter ↓\
\ The foeman trod the shining hall-pavement, ↓\
\  ↓\
\ Strode he angrily; from the eyes of him glimmered ↓\
\ A lustre unlovely likest to fire. ↓\
\ He beheld in the hall the heroes in numbers, ↓\
\ A circle of kinsmen sleeping together, ↓\
\  ↓\
\ A throng of thanemen: then his thoughts were exultant, ↓\
\ He minded to sunder from each of the thanemen ↓\
\ The life from his body, horrible demon, ↓\
\ Ere morning came, since fate had allowed him ↓\
\  ↓\
\ The prospect of plenty. Providence willed not ↓\
\ To permit him any more of men under heaven ↓\
\ To eat in the night-time. Higelac's kinsman ↓\
\ Great sorrow endured how the dire-mooded creature ↓\
\ In unlooked-for assaults were likely to bear him. ↓\
\ No thought had the monster of deferring the matter, ↓\
\  ↓\
\ But on earliest occasion he quickly laid hold of ↓\
\ A soldier asleep, suddenly tore him, ↓\
\ Bit his bone-prison, the blood drank in currents, ↓\
\ Swallowed in mouthfuls: he soon had the dead man's ↓\
\ Feet and hands, too, eaten entirely. ↓\
\ Nearer he strode then, the stout-hearted warrior ↓\
\  ↓\
\ Snatched as he slumbered, seizing with hand-grip, ↓\
\ Forward the foeman foined with his hand; ↓\
\ Caught he quickly the cunning deviser, ↓\
\ On his elbow he rested. This early discovered ↓\
\ The master of malice, that in middle-earth's regions, ↓\
\ 'Neath the whole of the heavens, no hand-grapple greater ↓\
\  ↓\
\ In any man else had he ever encountered: ↓\
\ Fearful in spirit, faint-mooded waxed he, ↓\
\ Not off could betake him; death he was pondering, ↓\
\  ↓\
\ Would fly to his covert, seek the devils' assembly: ↓\
\ His calling no more was the same he had followed ↓\
\ Long in his lifetime. The liege-kinsman worthy ↓\
\  ↓\
\ Of Higelac minded his speech of the evening, ↓\
\ Stood he up straight and stoutly did seize him. ↓\
\ His fingers crackled; the giant was outward, ↓\
\ The earl stepped farther. The famous one minded ↓\
\ To flee away farther, if he found an occasion, ↓\
\ And off and away, avoiding delay, ↓\
\ To fly to the fen-moors; he fully was ware of ↓\
\ The strength of his grapple in the grip of the foeman! ↓\
\  ↓\
\ 'Twas an ill-taken journey that the injury-bringing, ↓\
\ Harrying harmer to Heorot wandered: ↓\
\  ↓\
\ The palace re-echoed; to all of the Danemen, ↓\
\ Dwellers in castles, to each of the bold ones, ↓\
\ Earlmen, was terror. Angry they both were, ↓\
\ Archwarders raging. Rattled the building; ↓\
\ 'Twas a marvellous wonder that the wine-hall withstood then ↓\
\ The bold-in-battle, bent not to earthward, ↓\
\ Excellent earth-hall; but within and without it ↓\
\ Was fastened so firmly in fetters of iron, ↓\
\ By the art of the armorer. Off from the sill there ↓\
\ Bent mead-benches many, as men have informed me, ↓\
\ Adorned with gold-work, where the grim ones did struggle. ↓\
\ The Scylding wise men weened ne'er before ↓\
\ That by might and main-strength a man under heaven ↓\
\ Might break it in pieces, bone-decked, resplendent, ↓\
\ Crush it by cunning, unless clutch of the fire ↓\
\ In smoke should consume it. The sound mounted upward ↓\
\  ↓\
\ Novel enough; on the North Danes fastened ↓\
\ A terror of anguish, on all of the men there ↓\
\ Who heard from the wall the weeping and plaining, ↓\
\ The song of defeat from the foeman of heaven, ↓\
\ Heard him hymns of horror howl, and his sorrow ↓\
\ Hell-bound bewailing. He held him too firmly ↓\
\ Who was strongest of main-strength of men of that era. ↓\
\  ↓\
\ For no cause whatever would the earlmen's defender ↓\
\ Leave in life-joys the loathsome newcomer, ↓\
\ He deemed his existence utterly useless ↓\
\ To men under heaven. Many a noble ↓\
\ Of Beowulf brandished his battle-sword old, ↓\
\ Would guard the life of his lord and protector, ↓\
\ The far-famous chieftain, if able to do so; ↓\
\ While waging the warfare, this wist they but little, ↓\
\ Brave battle-thanes, while his body intending ↓\
\  ↓\
\ To slit into slivers, and seeking his spirit: ↓\
\ That the relentless foeman nor finest of weapons ↓\
\ Of all on the earth, nor any of war-bills ↓\
\ Was willing to injure; but weapons of victory ↓\
\ Swords and suchlike he had sworn to dispense with. ↓\
\ His death at that time must prove to be wretched, ↓\
\ And the far-away spirit widely should journey ↓\
\ Into enemies' power. This plainly he saw then ↓\
\ Who with mirth of mood malice no little ↓\
\ Had wrought in the past on the race of the earthmen ↓\
\ To God he was hostile, that his body would fail him, ↓\
\ But Higelac's hardy henchman and kinsman ↓\
\ Held him by the hand; hateful to other ↓\
\  ↓\
\ Was each one if living. A body-wound suffered ↓\
\ The direful demon, damage incurable ↓\
\  ↓\
\ Was seen on his shoulder, his sinews were shivered, ↓\
\ His body did burst! To Beowulf was given ↓\
\ Glory in battle; Grendel from thenceward ↓\
\ Must flee and hide him in the fen-cliffs and marshes, ↓\
\ Sick unto death, his dwelling must look for ↓\
\ Unwinsome and woful; he wist the more fully ↓\
\  ↓\
\ The end of his earthly existence was nearing, ↓\
\ His life-days' limits. At last for the Danemen, ↓\
\ When the slaughter was over, their wish was accomplished. ↓\
\ The comer-from-far-land had cleansed then of evil, ↓\
\ Wise and valiant, the war-hall of Hrothgar, ↓\
\ Saved it from violence. He joyed in the night-work, ↓\
\ In repute for prowess; the prince of the Geatmen ↓\
\ For the East-Danish people his boast had accomplished, ↓\
\ Bettered their burdensome bale-sorrows fully, ↓\
\ The craft-begot evil they erstwhile had suffered ↓\
\ And were forced to endure from crushing oppression, ↓\
\ Their manifold misery. 'Twas a manifest token, ↓\
\  ↓\
\ When the hero-in-battle the hand suspended, ↓\
\ The arm and the shoulder, there was all of the claw ↓\
\ Of Grendel together, 'neath great-stretching hall-roof. ↓\
\  ↓\
\ In the mist of the morning many a warrior ↓\
\ Stood round the gift-hall, as the story is told me: ↓\
\ Folk-princes fared then from far and from near ↓\
\ Through long-stretching journeys to look at the wonder, ↓\
\ The footprints of the foeman. Few of the warriors ↓\
\  ↓\
\ Who gazed on the foot-tracks of the inglorious creature ↓\
\ His parting from life pained very deeply, ↓\
\ How, weary in spirit, off from those regions ↓\
\ In combats conquered he carried his traces, ↓\
\ Fated and flying, to the flood of the nickers. ↓\
\  ↓\
\ There in bloody billows bubbled the currents, ↓\
\ The angry eddy was everywhere mingled ↓\
\ And seething with gore, welling with sword-blood; ↓\
\ He death-doomed had hid him, when reaved of his joyance ↓\
\ He laid down his life in the lair he had fled to, ↓\
\ His heathenish spirit, where hell did receive him. ↓\
\ Thence the friends from of old backward turned them, ↓\
\ And many a younker from merry adventure, ↓\
\ Striding their stallions, stout from the seaward, ↓\
\ Heroes on horses. There were heard very often ↓\
\  ↓\
\ Beowulf's praises; many often asserted ↓\
\ That neither south nor north, in the circuit of waters, ↓\
\  ↓\
\ O'er outstretching earth-plain, none other was better ↓\
\ 'Mid bearers of war-shields, more worthy to govern, ↓\
\ 'Neath the arch of the ether. Not any, however, ↓\
\ 'Gainst the friend-lord muttered, mocking-words uttered ↓\
\  ↓\
\ Of Hrothgar the gracious (a good king he). ↓\
\ Oft the famed ones permitted their fallow-skinned horses ↓\
\ To run in rivalry, racing and chasing, ↓\
\ Where the fieldways appeared to them fair and inviting, ↓\
\ Known for their excellence; oft a thane of the folk-lord, ↓\
\  ↓\
\ A man of celebrity, mindful of rhythms, ↓\
\ Who ancient traditions treasured in memory, ↓\
\ New word-groups found properly bound: ↓\
\ The bard after 'gan then Beowulf's venture ↓\
\  ↓\
\ Wisely to tell of, and words that were clever ↓\
\ To utter skilfully, earnestly speaking, ↓\
\ Everything told he that he heard as to Sigmund's ↓\
\  ↓\
\ Mighty achievements, many things hidden, ↓\
\ The strife of the Waelsing, the wide-going ventures ↓\
\ The children of men knew of but little, ↓\
\ The feud and the fury, but Fitela with him, ↓\
\ When suchlike matters he minded to speak of, ↓\
\ Uncle to nephew, as in every contention ↓\
\ Each to other was ever devoted: ↓\
\ A numerous host of the race of the scathers ↓\
\ They had slain with the sword-edge. To Sigmund accrued then ↓\
\ No little of glory, when his life-days were over, ↓\
\ Since he sturdy in struggle had destroyed the great dragon, ↓\
\ The hoard-treasure's keeper; 'neath the hoar-grayish stone he, ↓\
\ The son of the atheling, unaided adventured ↓\
\ The perilous project; not present was Fitela, ↓\
\ Yet the fortune befell him of forcing his weapon ↓\
\ Through the marvellous dragon, that it stood in the wall, ↓\
\ Well-honored weapon; the worm was slaughtered. ↓\
\ The great one had gained then by his glorious achievement ↓\
\ To reap from the ring-hoard richest enjoyment, ↓\
\ As best it did please him: his vessel he loaded, ↓\
\ Shining ornaments on the ship's bosom carried, ↓\
\ Kinsman of Waels: the drake in heat melted. ↓\
\  ↓\
\ He was farthest famed of fugitive pilgrims, ↓\
\ Mid wide-scattered world-folk, for works of great prowess, ↓\
\ War-troopers' shelter: hence waxed he in honor. ↓\
\  ↓\
\ Afterward Heremod's hero-strength failed him, ↓\
\ His vigor and valor. 'Mid venomous haters ↓\
\ To the hands of foemen he was foully delivered, ↓\
\ Offdriven early. Agony-billows ↓\
\  ↓\
\ Oppressed him too long, to his people he became then, ↓\
\ To all the athelings, an ever-great burden; ↓\
\ And the daring one's journey in days of yore ↓\
\ Many wise men were wont to deplore! ↓\
\ Such as hoped he would bring them help in their sorrow, ↓\
\ That the son of their ruler should rise into power, ↓\
\ Holding the headship held by his fathers, ↓\
\ Should govern the people, the gold-hoard and borough, ↓\
\ The kingdom of heroes, the realm of the Scyldings. ↓\
\  ↓\
\ He to all men became then far more beloved, ↓\
\ Higelac's kinsman, to kindreds and races, ↓\
\ To his friends much dearer; him malice assaulted.-- ↓\
\  ↓\
\ Oft running and racing on roadsters they measured ↓\
\ The dun-colored highways. Then the light of the morning ↓\
\ Was hurried and hastened. Went henchmen in numbers ↓\
\ To the beautiful building, bold ones in spirit, ↓\
\ To look at the wonder; the liegelord himself then ↓\
\ From his wife-bower wending, warden of treasures, ↓\
\ Glorious trod with troopers unnumbered, ↓\
\ Famed for his virtues, and with him the queen-wife ↓\
\ Measured the mead-ways, with maidens attending. "
