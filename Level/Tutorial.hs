module Level.Tutorial where

import GPL
import Types

-- This is the whole level; tutorial plus some padding.
level :: Level
level = tutorial ++ gpl ++
	[ ""
	, "                   ~ ~ ~ ~ ~"
	, " *                                                   *"
	, " * And yes, that's the license of Scroll!            *"
	, " * Copyright 2015 by Joey Hess, licensed under GPL2+ *"
	, " *                                                   *"
	, "                   ~ ~ ~ ~ ~"
	, ""
	, ""
	, ""
	-- clear scrolling is a pun on mariner1
	, "Oh, look... Clear scrolling ahead! Can't be far to the end now!"
	, ""
	, ""
	, ""
	, ""
	, ""
	, ""
	, ""
	, ""
	, ""
	, ""
	, ""
	, ""
	, ""
	, ""
	, ""
	, ""
	]

{- Note that this text is intentionally written with straight-through
 - paths of whitespace! (Marked by #) Avoid edits that close them off.
 - Toward the bottom, the user has to start using spells to get by,
 - starting with 'o'. There is also an opportunity to cast the "new"
 - spell, twice. (And the GPL contains several other "new"'s.)
 - -}
tutorial :: Level
tutorial = map (replace '#' ' ')
	[ "||                                                        ||"
	, "|        You've always been a bookworm.                    ||"
	, "|                                                         ||"
	, "You love to read, to explore#the magic of words,"
	, "to soak them up,            #"
	, "Slipping#between letters with#ease and grace."
	, "        #####################################"
	, "But, you fear you may be stuck here, on this#scroll."
	, "                                            ##"
	, "You landed here by accident, perusing volumes#in the"
	, "Great Library. Books are better than scrolls;#they've"
	, "nooks and crannies in their bindings, and you#can"
	, "pull yourself between pages with ease.       #"
	, "                           ###################"
	, "Not like this scroll, which#despite the value of its"
	, "words, is no fit home for a#self-respecting bookworm."
	, "This tattered scroll, which#rolls up crushingly tight"
	, "upon itself above.         #"
	, "                   #########"
	, "How archaic; what a#cruel fate awaits if you don't"
	, "keep on the move...#"
	, "       #############"
	, "Perhaps#the bottom of the scroll will be more hospitable"
	, "to you,#or offer a way off. Or perhaps there you will die?"
	, "     ###"
	, "   Oh#no!"
	, "   Is#it going to move again, so soon?"
	, "     ###########################"
	, "In this situation, it's hard to#remember the spells"
	, "you've learned before, browsing#seas of shining words."
	, "                           #####"
	, "One that comes to mind is \"v\", great when you have"
	, "too many letters stuck in your stomach. (Greedy worm!)" -- v -> clear
	, "It can get you past many problems."
	, "                           ######"
	, "    Some spells are longer, and quite hard to find,"
	, "like \"new\", which magics up a new way to cast a spell,"
	, "     out of other letters you've consumed." -- new + k + v -> q = v
	, "   ##############################"
	, "Oh,#that's a powerful one indeed!"
	, "   #                                        |"
	, "   ###################                       |"
	, "And while letters scribed in this archaic ink" 
	, "don't nourish you, a quick snack of something" -- q -> clear
	, "smaller may give you time to gather your wits,"
	, "and bring a spell to mind...      " -- ... is a hint
	, "                                 |"
	, "At least this old crumbling parchment has lots of" -- must dive
	, "little holes, so it's easy to squeeze through from"
	, "front to back, and back again. "
	, "              |"
	, "                   ~ ~ ~ ~ ~ ~"
	, ""
	]

replace :: Char -> Char -> String -> String
replace old new = map $ \c ->
	if c == old
		then new
		else c
