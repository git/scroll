-- This file is in the public domain.

module Level.Coleridge where

import Types
import Level.Shuffle
import Level.Tutorial
import Rand

-- We start the level with the scroll already partly read;
-- somewhere in the middle of mariner. Exact point is
-- random, as is order of mariner's stanzas.
--
-- The user will probably flip to the help side,
-- and get through the tutorial easily. Then they will
-- eventually get stuck somewhere in the GPL, and flip
-- back to this side.
--
-- kubla is more interesting, being line-randomized
-- and harder, so it's nice if the user tends to end
-- up in it when they flip back. Or, with it just
-- coming into view. So, the amount of mariner that's
-- used is randomized with that in mind.
level :: Rand -> Level
level r = concat
	[ final (length tutorial + extra) $ concat $ rand poem
	, concat $ rand conclusion
	, concatMap rand kubla
	]
  where
	rand = withStdGen r asis shuffle
	extra = withStdGen r 10 (fst . randomR (1,20))
	(poem, conclusion) = poems !! (withStdGen r 0 $ fst . randomR (0,length poems-1))
	final n = reverse . take n . reverse
	poems =
		[ mariner1
		, mariner2
		]

kubla :: [Section String]
kubla = 
	[ FixedSection
	  [ ""
	  , "In Xanadu did Kubla Khan            |"
	  , "A stately pleasure-dome decree:"
	  , "Where Alph, the sacred river, ran"
	  , "Through caverns measureless to man"
	  , "  Down to a sunless sea."
	  ]
	, ShuffleableSection
	  [ "So twice five miles of fertile ground"
	  , "With walls and towers were girdled round;"
	  , "And there were gardens bright with sinuous rills,"
	  , "Where blossomed many an incense-bearing tree;"
	  , "And here were forests ancient as the hills,"
	  , "Enfolding sunny spots of greenery."
	  ]
	, FixedSection [""]
	, ShuffleableSection
	  [ "But oh! that deep romantic chasm which slanted"
	  , "Down the green hill athwart a cedarn cover!"
	  , "A savage place! as holy and enchanted"
	  , "As e’er beneath a waning moon was haunted"
	  , "By woman wailing for her demon-lover!"
	  , "And from this chasm, with ceaseless turmoil seething,"
	  , "As if this earth in fast thick pants were breathing,"
	  , "A mighty fountain momently was forced:"
	  , "Amid whose swift half-intermitted burst"
	  , "Huge fragments vaulted like rebounding hail,"
	  , "Or chaffy grain beneath the thresher’s flail:"
	  , "And mid these dancing rocks at once and ever"
	  , "It flung up momently the sacred river."
	  , "Five miles meandering with a mazy motion"
	  , "Through wood and dale the sacred river ran,"
	  , "Then reached the caverns measureless to man,"
	  , "And sank in tumult to a lifeless ocean;"
	  ]
	, FixedSection
	  [ "And ’mid this tumult Kubla heard from far"
	  , "Ancestral voices prophesying war!"
	  ]
	, ShuffleableSection
	  [ "  The shadow of the dome of pleasure"
	  , "  Floated midway on the waves;"
	  , "  Where was heard the mingled measure"
	  , "  From the fountain and the caves."
	  ]
	, FixedSection
	  [ "It was a miracle of rare device,"
	  , "A sunny pleasure-dome with caves of ice!"
	  , ""
	  ]
	, ShuffleableSection
	  [ "   A damsel with a dulcimer"
	  , "   In a vision once I saw:"
	  , "   It was an Abyssinian maid"
	  , "   And on her dulcimer she played,"
	  , "   Singing of Mount Abora."
	  , "   Could I revive within me"
	  , "   Her symphony and song,"
	  , "   To such a deep delight ’twould win me,"
	  ]
	, FixedSection
	  [ ""
	  , "That with music loud and long,"
	  , "I would build that dome in air,"
	  ]
	, ShuffleableSection
	  [ "That sunny dome! those caves of ice!"
	  , "And all who heard should see them there,"
	  , "And all should cry, Beware! Beware!"
	  , "His flashing eyes, his floating hair!"
	  , "Weave a circle round him thrice,"
	  ]
	, FixedSection
	  [ "And close your eyes with holy dread"
	  , "For he on honey-dew hath fed,"
	  , "And drunk the milk of Paradise."
	  ]
	, FixedSection
	  [ ""
	  , "|    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~       |"
	  , "|                                  ||"
	  , "Then all the charm                  ||"
	  , "Is broken -- all that phantom-world so fair"
	  , "Vanishes, and a thousand circlets spread,"
	  , "And each mis-shape the other."
	  ]
	]

mariner1 :: (Section [String], Section [String])
mariner1 = (mariner1body, mariner1conclusion)

-- Short lines, so not too many choices, and easier to get stuck,
-- but not too long paragraphs.
mariner1body :: Section [String]
mariner1body = ShuffleableSection $ map (\l -> ("" : l))
	[ [ "It is an ancient Mariner,"
	  , "And he stoppeth one of three."
	  , "'By thy long grey beard and glittering eye,"
	  , "Now wherefore stopp'st thou me?'"
	  ]
	, [ "The Bridegroom's doors are opened wide,"
	  , "And I am next of kin;"
	  , "The guests are met, the feast is set:"
	  , "May'st hear the merry din.'"
	  ]
	, [ "He holds him with his skinny hand,"
	  , "'There was a ship,' quoth he."
	  , "'Hold off! unhand me, grey-beard loon!'"
	  , "Eftsoons his hand dropt he."
	  ]
	, [ "He holds him with his glittering eye-"
	  , "The Wedding-Guest stood still,"
	  , "And listens like a three years' child:"
	  , "The Mariner hath his will."
	  ]
	, [ "The Wedding-Guest sat on a stone:"
	  , "He cannot choose but hear;"
	  , "And thus spake on that ancient man,"
	  , "The bright-eyed Mariner."
	  ]
	, [ "The ship was cheered, the harbour cleared,"
	  , "Merrily did we drop"
	  , "Below the kirk, below the hill,"
	  , "Below the lighthouse top."
	  ]
	, [ "The Sun came up upon the left,"
	  , "Out of the sea came he!"
	  , "And he shone bright, and on the right"
	  , "Went down into the sea."
	  ]
	, [ "'Higher and higher every day,"
	  , "Till over the mast at noon-'"
	  , "The Wedding-Guest here beat his breast,"
	  , "For he heard the loud bassoon."
	  ]
	, [ "The bride hath paced into the hall,"
	  , "Red as a rose is she;"
	  , "Nodding their heads before her goes"
	  , "The merry minstrelsy."
	  ]
	, [ "The Wedding-Guest he beat his breast,"
	  , "Yet he cannot choose but hear;"
	  , "And thus spake on that ancient man,"
	  , "The bright-eyed Mariner."
	  ]
	, [ "And now the STORM-BLAST came, and he"
	  , "Was tyrannous and strong:"
	  , "He struck with his o'ertaking wings,"
	  , "And chased us south along."
	  ]
	, [ "With sloping masts and dipping prow,"
	  , "As who pursued with yell and blow"
	  , "Still treads the shadow of his foe,"
	  , "And forward bends his head,"
	  , "The ship drove fast, loud roared the blast,"
	  , "And southward aye we fled."
	  ]
	, [ "And now there came both mist and snow,"
	  , "And it grew wondrous cold:"
	  , "And ice, mast-high, came floating by,"
	  , "As green as emerald."
	  ]
	, [ "And through the drifts the snowy clifts"
	  , "Did send a dismal sheen:"
	  , "Nor shapes of men nor beasts we ken-"
	  , "The ice was all between."
	  ]
	, [ "The ice was here, the ice was there,"
	  , "The ice was all around:"
	  , "It cracked and growled, and roared and howled,"
	  , "Like noises in a swound!"
	  ]
	, [ "At length did cross an Albatross,"
	  , "Thorough the fog it came;"
	  , "As if it had been a Christian soul,"
	  , "We hailed it in God's name."
	  ]
	, [ "It ate the food it ne'er had eat,"
	  , "And round and round it flew."
	  , "The ice did split with a thunder-fit;"
	  , "The helmsman steered us through!"
	  ]
	, [ "And a good south wind sprung up behind;"
	  , "The Albatross did follow,"
	  , "And every day, for food or play,"
	  , "Came to the mariner's hollo!"
	  ]
	, [ "In mist or cloud, on mast or shroud,"
	  , "It perched for vespers nine;"
	  , "Whiles all the night, through fog-smoke white,"
	  , "Glimmered the white Moon-shine."
	  ]
	]

mariner1conclusion :: Section [String]
mariner1conclusion = FixedSection $ map (\l -> ("" : l))
	[ [ "'God save thee, ancient Mariner!"
	  , "From the fiends, that plague thee thus!-"
	  , "Why look'st thou so?'-With my cross-bow"
	  , "I shot the ALBATROSS. "
	  ]
	, [ "|               ~ ~ ~                   |"
	  ]
	]

mariner2 :: (Section [String], Section [String])
mariner2 = (mariner2body, mariner2conclusion)

mariner2body :: Section [String]
mariner2body = ShuffleableSection $ map (\l -> ("" : l))
	[ [ "The Sun now rose upon the right:"
	  , "Out of the sea came he,"
	  , "Still hid in mist, and on the left"
	  , "Went down into the sea."
	  ]
	, [ "And the good south wind still blew behind,"
	  , "But no sweet bird did follow,"
	  , "Nor any day for food or play"
	  , "Came to the mariner's hollo!"
	  ]
	, [ "And I had done a hellish thing,"
	  , "And it would work 'em woe:"
	  , "For all averred, I had killed the bird"
	  , "That made the breeze to blow."
	  , "Ah wretch! said they, the bird to slay,"
	  , "That made the breeze to blow!"
	  ]
	, [ "Nor dim nor red, like God's own head,"
	  , "The glorious Sun uprist:"
	  , "Then all averred, I had killed the bird"
	  , "That brought the fog and mist."
	  , "'Twas right, said they, such birds to slay,"
	  , "That bring the fog and mist."
	  ]
	, [ "The fair breeze blew, the white foam flew,"
	  , "The furrow followed free;"
	  , "We were the first that ever burst"
	  , "Into that silent sea."
	  ]
	, [ "Down dropt the breeze, the sails dropt down,"
	  , "'Twas sad as sad could be;"
	  , "And we did speak only to break"
	  , "The silence of the sea!"
	  ]
	, [ "All in a hot and copper sky,"
	  , "The bloody Sun, at noon,"
	  , "Right up above the mast did stand,"
	  , "No bigger than the Moon."
	  ]
	, [ "Day after day, day after day,"
	  , "We stuck, nor breath nor motion;"
	  , "As idle as a painted ship"
	  , "Upon a painted ocean."
	  ]
	, [ "Water, water, every where,"
	  , "And all the boards did shrink;"
	  , "Water, water, every where,"
	  , "Nor any drop to drink."
	  ]
	, [ "The very deep did rot: O Christ!"
	  , "That ever this should be!"
	  , "Yea, slimy things did crawl with legs"
	  , "Upon the slimy sea."
	  ]
	, [ "About, about, in reel and rout"
	  , "The death-fires danced at night;"
	  , "The water, like a witch's oils,"
	  , "Burnt green, and blue and white."
	  ]
	, [ "And some in dreams assurèd were"
	  , "Of the Spirit that plagued us so;"
	  , "Nine fathom deep he had followed us"
	  , "From the land of mist and snow."
	  ]
	, [ "And every tongue, through utter drought,"
	  , "Was withered at the root;"
	  , "We could not speak, no more than if"
	  , "We had been choked with soot."
	  ]
	]
		
mariner2conclusion :: Section [String]
mariner2conclusion = FixedSection $ map (\l -> ("" : l))
	[ [ "Ah! well a-day! what evil looks"
	  , "Had I from old and young!"
	  , "Instead of the cross, the Albatross"
	  , "About my neck was hung. "
	  ]
	, [ "|               ~ ~ ~                   |"
	  ]
	]
