module Level.File (level) where

import Types
import Level.Shuffle
import qualified Level.Tutorial

level :: Rand -> String -> (Level, Level)
level r s = (rand $ ShuffleableSection (lines s), Level.Tutorial.level)
  where
	rand = case r of
		Rand g -> shuffle g
		_ -> asis
