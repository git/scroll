module Term where

import UI.NCurses (Event(..))

data InputEvent = InputEvent Event

inputCharacter :: InputEvent -> Maybe Char
inputCharacter (InputEvent (EventCharacter c)) = Just c
inputCharacter _ = Nothing
