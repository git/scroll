module WorldSetup where

import Control.Monad.ST
import Control.Monad.State.Strict
import qualified Data.Vector as V
import qualified Data.Set as S
import Data.Vector ((!))
import Data.Default
import Control.Applicative
import Prelude

import Types
import Level
import Level.Border
import Player
import Spell
import Poison
import Rand

-- Creates a new game world from the specified level content.
makeWorld :: (Level, Level) -> Integer -> Rand -> ST RealWorld S
makeWorld (frontl, backl) screenheight r = do
	(front, frontbuf) <- setup frontv
	(back, backbuf) <- setup backv
	addCap front
	addCap back
	let s = S
		{ world = front
		, flipSide = back
		, player = def { playerSpells = startingspells }
		, bottomBuffer = (frontbuf, backbuf)
		, topBuffer = 0
		, peruser = def
		, randSource = r
		, helpShown = False
		, messages = []
		, spells = allSpells
		, poisons = allPoisons
		, windows = []
		}
	execStateT startingPosition s
  where
	(frontv, backv) = levelVectors (frontl, backl)

	-- 2 for each end of scroll
	scrollsize = fromIntegral $ screenheight - 4

	setup :: V.Vector (V.Vector Char) -> ST RealWorld (World, Vec2 Char)
	setup v = do
		mv <- V.mapM V.thaw v
		let (w, b) = V.splitAt (scrollsize - 1) mv
		-- grow for scroll cap
		let p = V.singleton (w ! 0)
		let w' = V.concat [p,p,w,p,p]
		(,)
			<$> V.thaw w'
			<*> V.thaw b

	startingspells = flip S.insert startingSpells $
		optspells !! fst (randomR (0, length optspells - 1) (randGen r))
	optspells = S.toList maybeStartingSpells
