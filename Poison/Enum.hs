module Poison.Enum where

data PoisonEnum
	= PoisonMold
	| PoisonStunner
	| PoisonFungus
	deriving (Show, Eq, Ord)
